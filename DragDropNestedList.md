# Test Task

## View
![Scheme](http://i.piccy.info/i9/95cc2f195e49480d786b3e25e9bd7e9c/1525366092/12752/1241594/download_1_.png) 

![Scheme](http://i.piccy.info/i9/16c9f3788cc251cfc411ca36ad1a9b04/1525366422/14463/1241594/download_2_.png)

## Requirements

*   Component should support all props defined in the section `Props`
*   It should be possible to use the component with the example in section `Usage`
*   Add Readme of this component with its usage example and props/api
*   Component should use `React DnD` in order to get drag&drop functionality https://github.com/react-dnd/react-dnd
*   Component should use [MultiBackend](https://www.npmjs.com/package/react-dnd-multi-backend) with [MouseBackend](https://www.npmjs.com/package/react-dnd-mouse-backend) and/or [TouchBackend](https://www.npmjs.com/package/react-dnd-touch-backend) in order to support both mouse and touch drag & drop
*   If mouse is over a draggable elements `drag` cursor should be used
*   On drag and dragged element is outside of <Nestable /> container, use `dragging` cursor
*   On drag, dragged element is inside of <Nestable /> container and can be dropped into target place, use `copy` cursor
*   Component should not have any UI specific styles (only styles related to business logic)
*   Component should be implemented from scratch with the use of provided libraries 👆

<sub>See https://github.com/echenley/react-dnd-nestable for implementation reference. Take a look to the use of react-dnd</sub>

## Usage

```es6
class Demo extends Component {
    state = {
        items: [
            {
                id: 'uuid-1',
                name: 'Page 1',
                href: 'http://link.to/page-1',
                order: '1'
            },
            {
                id: 'uuid-2',
                name: 'Page 2',
                href: 'http://link.to/page-2',
                order: '2'
            },
            {
                id: 'uuid-3',
                name: 'Page 3',
                href: 'http://link.to/page-3',
                order: '3'
            },
            {
                id: 'uuid-3.1',
                name: 'Page 3.1',
                href: 'http://link.to/page-4',
                order: '3.1'
            },
            {
                id: 'uuid-3.2',
                name: 'Page 3.2',
                href: 'http://link.to/page-4',
                order: '3.2'
            },
            {
                id: 'uuid-3.1.1',
                name: 'Page 3.1.1',
                href: 'http://link.to/page-4',
                order: '3.1.1'
            },
            {
                id: 'uuid-4',
                name: 'Page 4',
                href: 'http://link.to/page-5',
                order: '4'
            },
            {
                id: 'uuid-5',
                name: 'Page 5',
                href: 'http://link.to/page-5',
                order: '5'
            },
            {
                id: 'uuid-6',
                name: 'Page 6',
                href: 'http://link.to/page-5',
                order: '6'
            },
            {
                id: 'uuid-7',
                name: 'Page 7',
                href: 'http://link.to/page-5',
                order: '7'
            }
        ]
    };

    renderItem = (item, nestable) => {
        const draggingClass = nestable.isDragging ? ' is-dragging' : '';
        const allowedClass = nestable.isDropAllowed ? ' is-allowed' : '';

        return (
            <div className="container">
                <div
                    className={
                        'ws-page-manager-item' + draggingClass + allowedClass
                    }
                >
                    {nestable.hasChildren && (
                        <div
                            className="ws-page-manage-item-toggle"
                            onClick={nestable.toggle}
                        >
                            {nestable.isCollapsed ? '+' : '-'}
                        </div>
                    )}

                    <a href={item.href}>{item.name}</a>
                </div>
            </div>
        );
    };

    render() {
        return (
            <div className="nestable-demo-container">
                <NestableStyled
                    items={this.state.items}
                    renderItem={this.renderItem}
                    collapsed={['uuid-3']}
                    onChange={(item, items) => console.log(item, items)}
                />
            </div>
        );
    }
}
```

## Props

| Name              | Type                                                                                           | Default                       | Description                                                                                                                 |
| ----------------- | ---------------------------------------------------------------------------------------------- | ----------------------------- | --------------------------------------------------------------------------------------------------------------------------- |
| items             | array                                                                                          | []                            | Array of items. Every item must be of shape { id: @uniq, order: /(\d+\.?)+/ }                                               |
| renderItem        | function                                                                                       | ({ item }) => item.toString() | Function for rendering every item. Gets `item` and `nestable` objects as params.                                            |
| maxDepth          | number                                                                                         | 3                             | Maximum nesting level                                                                                                       |
| threshold         | number                                                                                         | 30                            | Amount of pixels which mouse should move horizontally before increasing/decreasing level (nesting) of current element.      |
| collapsed         | one of: `none`, `all`, `[ "id1", "id2", ... "idN"] |`all` | collapse specific items by default |
| onChange          | function                                                                                       |                               | Gets called on list change. Receives 2 params: affected item, updated items array.                                          |
| customDragHandler | boolean                                                                                        | false                         | If true, passed additional `nestable.connectDragSource` function into `renderItem`. Allows to create a custom drag handler. |

## Nestable Object

| Name              | Type     | Default | Description                                                                                              |
| ----------------- | -------- | ------- | -------------------------------------------------------------------------------------------------------- |
| isDragging        | boolean  | false   | True if nestable item is being dragged                                                                   |
| isCollapsed       | boolean  | false   | True if nestable item is in `dragged` state and its possible to drop it in current area                  |
| hasChildren       | boolean  | false   | True if nestable has nested children                                                                     |
| toggle            | function |         | collapse/expand current nestable item. Does nothing if nestable has no children                          |
| collapse          | function |         | collapse current nestable item. Does nothing if nestable has no children                                 |
| expand            | function |         | expand current nestable item. Does nothing if nestable has no children                                   |
| connectDragSource | function |         | use in order to create custom drag handler. Available if `customDragHandler` is passed to `<Nestable />` |


