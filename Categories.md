# Nested List

## View
![Scheme](http://i.piccy.info/i9/fede0ef0e8c80be8a7928b75d32ede19/1530018083/24580/1253931/15_layers_no_collaps.png) 

## API 
[template-categories](https://api.animatron.com/template-categories)

## Requirements

*   Add Readme of this component with its usage example and props/api
*   Component should use [React](https://github.com/facebook/react/) in order to render UI
*   Component should use [Axios](https://github.com/axios/axios) in order to fetch list of categories
*   Component should be implemented from scratch with the use of provided libraries

## Additional(not required for the task)

*   Use TypeScript
*   Add possibility to collapse nested list

![Scheme](http://i.piccy.info/i9/f032343f0b28942ef3cc7a3f7a2068af/1530018066/24855/1253931/15_layers.png) 