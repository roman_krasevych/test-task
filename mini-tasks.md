## 1) Run the promises in parallel and sequence

```
const createPromise = (...arg) => new Promise((r) => setTimeout(() => r(arg), 100))
const promises = [createPromise(1),createPromise(2),createPromise(3)]
```

### Answer

```
async function sequence(promises) {
  const result = [];
  for (const p of promises) {
    const res = await p;
    result.push(res);
  }
  return result;
}
```



## 2) Create function that cancel promise after timeout

```
const createPromise = (...arg) => new Promise((r) => setTimeout(() => r(arg), 500))
promiseDelay(createPromise('good'), 1000).then(console.log)
```

### Asnwer

```
const promiseDelay = (promise, ms) => {
  const timeout = new Promise((_, r) =>
    setTimeout(() => r('request timeout'), ms)
  );
  return Promise.race([promise, timeout]);
};
```
