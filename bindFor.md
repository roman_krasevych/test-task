You should implement bindFor function and add types with typescript, below, you can see some example how we used that bindFor.

// helpers.ts
```js
type SendRequest = (config: any) => Promise<any>;

const bindFor = 
```

//  auth.ts
```js
type SignUpData = {
  password: string;
  displayName: string;
}
const signup = (request: SendRequest) => (data: SignUpData) =>
  request({
    data,
    url: 'signup',
    method: 'POST'
  });


type LoginData = {
  secretCode: number;
  email: string;
}
const login = (request: SendRequest) => (data: LoginData) =>
  request({
    data,
    url: 'login',
    method: 'POST'
  });
```

// api.ts
```js
const request = (...arg) => Promise.resolve(arg).then(console.log);
const auth = bindFor(request)({ login, signup });
auth.signup({ password: 11, displayName: '232' })
auth.login({email: '', secretCode: 111})
```